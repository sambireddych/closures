import UIKit

// Answer each question in the space that follows it. Most of these questions involve closures, a few are just to test verify your knowledge of other subjects.


// 0. Write a Void function, fillIt(n:min:max:data:) with 4 parameters, n, min, max and data. The first 3 will be Ints, the 4th an [Int]. It will cause the 4th parameter to be filled with n Ints, between min and max inclusive, generated at random. Hint: For a parameter to changer its value, as data does, it must include a keyword that we talked about in the first 2 weeks or so of class.

func fillIt(n:Int, min:Int, max:Int, data:inout [Int]){
    for _ in 0..<n{
        let randomnumbers = Int.random(in: min...max)
        data.append(randomnumbers)
    }
}


var data:[Int] = [23, 25]
fillIt(n: 15, min: 5, max: 10, data: &data)
// 1. Invoke fillIt() with values 15, 5, 10 -- and pass in data as well




// 2. Create a struct, Mouse, with stored properties for x, y, and batteryLife, all Doubles. x and y can have any value, batteryLife should print a warning message if the value being assigned is outside the range 0.0-5.0 inclusive (hint: look at at the Restaurant class covered in class on 26 Feb). Mouse should adhere to the Equatable and CustomStringConvertible protocols. Two Mice are equal if they have the same property values. Implement CustomStringConvertible so it prints out a String that looks like the following (replace numbers with actual values, to 1 decimal, and use "Good" if battery life is at least 2.5, "Poor" otherwise:
//
//  Position: (5.0, 3.0), Battery Life: Good

struct Mouse: Equatable, CustomStringConvertible{
    
    
    var x:Double
    var y:Double
    var batteryLife:Double{
        willSet {
            if newValue < 0.0 || newValue > 5.0 {
                print("Warning")
            }
        }
    }
    var description: String{
        if(batteryLife >= 2.5){
            return "position: \(x, y), Battery Life: Good"
        }
        else {return "position: \(x, y), Battery Life: Poor"}
    }
    
    static func == (lhs: Mouse, rhs: Mouse) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y && lhs.batteryLife == rhs.batteryLife
    }

}






// 3. Store three Mice in an array, cage (sorry, Mice). Make the first and third elements equal, with a batteryLife == 5.0, the middle one with a value 2.0

var cage:[Mouse] = []
cage.append(Mouse( x: 1.0, y: 4.0, batteryLife: 5.0))
cage.append(Mouse( x: 0.0, y: 4.0, batteryLife: 2.0))
cage.append(Mouse( x: 1.0, y: 4.0, batteryLife: 5.0))

// 4. See if cage contains a Mouse with the same values as cage[1] -- print out true if it does, false otherwise. Hint: Remember that arrays define the contains() method ... it's that easy!
if cage.contains(cage[1]) {
    print("true")
}
else{
    print("false")
}

// 5. Write a function, quadrant(mouse:) to return a String "NE", "NW", "SE", "SW" or "ON AXIS" depending on where a Mouse is positioned relative to the origin. (e.g., if it is in the top-right quadrant, return "NE"; top-left quadrant, "NW"; bottom-left quadrant, "SW"; bottom-right quadrant, "SE". [You have already done a question like this.]


func qudrant(mouse:Mouse) -> String{
    if(mouse.x>0 && mouse.y>0){
        return "NE"
    }
    else if(mouse.x<0 && mouse.y>0){
        return "NW"
    }
    else if(mouse.x<0 && mouse.y<0){
        return "SW"
    }
    else if(mouse.x>0 && mouse.y<0){
        return "SE"
    }
    else{
        return "ON AXIS"
    }
}

// 6. Assign new values to cage so its first Mouse is in NE, second in NW, third in SW, fourth in SE and 5th on an axis


cage.append(Mouse(x: 2.0, y: 3.6, batteryLife: 2.0))

cage.append(Mouse(x: -1.0, y: 3.6, batteryLife: 3.2))

cage.append(Mouse(x: -2.0, y: -3.6, batteryLife: 4.1))

cage.append(Mouse(x: 2.0, y: -2.6, batteryLife: 1.9))

cage.append(Mouse(x: 0.0, y: 0.0, batteryLife: 2.3))


// 7. Map cage using quadrant, and print out the resulting array. [Hint: look at the map() function examples that you did in class on 28 Feb]


var cageMap = cage.map(qudrant)
print(cageMap)

// 8. Sort the elements of cage in increasing order of batteryLife, and print out the result. [Hint: See the sort(by:) example done in class on 28 Feb]

cage.sort() {$0.batteryLife < $1.batteryLife}

print(cage)


// 9. Write a method, splitter(data:classifier:) that will take an array of Ints, data, and a closure, classifier, that accepts an Int and returns a Bool. It will return a new array in whch all the elements for which the closure returned false will precede those elements for which the closure returned true.


func splitter(data:Int ..., Classifier:(Int) -> Bool) -> Array<Int>{
    var dataeven:[Int] = []
    var dataodd:[Int] = []
    for i in data{
        if Classifier(i){
            dataeven.append(i)
        }
        else{
            dataodd.append(i)
        }
    }
    return dataodd+dataeven
}



splitter(data: 10,15,20,23) { (x:Int) -> Bool in
    if x%2==0{
        return true
    }
    return false }

// For example, if data were [10, 15, 20, 23], and classifier returned true if an element were *even*, then splitter() would return (15, 23, 10, 20)

// Only use basic Swift programming constructs. Do not use any other function calls other than classsifier() and append()







// 10. Invoke splitter(), using data = [10, 15, 20, 23] and a classifier that returns true if an element is even. Print out the returned result.



// 11. Write a function, palindromic(num:) that will return true if its parameter, an Int, is palindromic (it reads the same backwards as forwards). For example "1221" is palindromic, whereas "1223" is not. [Hint: Convert an Int into a String. Then reverse it, and see if the two are equal.]

func palindrome(num:Int) -> Bool{
    let convertintInStr = String(num)
    let stringLength = convertintInStr.count
    var position = 0

    while position < stringLength / 2 {
        let startIndex = convertintInStr.index(convertintInStr.startIndex, offsetBy: position)
        let endIndex = convertintInStr.index(convertintInStr.endIndex, offsetBy: -position - 1)
        
        if convertintInStr[startIndex] == convertintInStr[endIndex] {
            position += 1
        } else {
            return false
        }
    }

    return true

}

palindrome(num: 1221)

// 12. Fill an array, aThou, with the numbers 1 ... 500 inclusive


let aThou = (1...500).map { (Int) -> Int in Int}
print(aThou)

// 13. Use filter to return those values that are palindromic
//
var filtered = aThou.filter(palindrome)
print(filtered)
